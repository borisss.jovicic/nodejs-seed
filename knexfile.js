const config = require('./services/wishlist/config');

module.exports = {
  client: 'mysql2',
  connection: {
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.name
  }
};
