const createUser = require('./create-user');
const asyncHandler = require('express-async-handler');
const { Router } = require('express');
const router = Router();
const authUser = require('./auth-user');
const authMiddleware = require('../../middleware/auth-middleware');

router.post(
  `/users`,
  asyncHandler(async (req, res) => {
    res.send(await createUser(req.body));
  })
);

router.post(
  `/users/auth`,
  authMiddleware,
  asyncHandler(async (req, res) => {
    res.send(await authUser(req.body));
  })
);

const userRouter = router;

module.exports = userRouter;
