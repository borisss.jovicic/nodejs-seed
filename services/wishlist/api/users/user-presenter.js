const db = require('../../repositories');

function mapUser(user) {
  // eslint-disable-next-line no-unused-vars
  const { password, ...rest } = user;
  return rest;
}

async function present(userIds) {
  const users = await db.users.getByIds(userIds);
  return users.map(mapUser);
}

module.exports = { present };
