const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('../../config');
const db = require('../../repositories');
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const authSchema = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().required()
}).required();

module.exports = async function authUser(data) {
  const { username, password } = Joi.attempt(data, authSchema);
  const user = await db.users.getByUsername(username);

  const passwordMatch = await bcrypt.compare(password, user.password);

  if (!passwordMatch) {
    throw Boom.notFound('User not found.');
  }

  const token = jwt.sign(
    {
      id: user.id
    },
    { key: config.jwt.privateKey, passphrase: config.jwt.passPhrase },
    { algorithm: 'RS256' }
  );

  return { user: user, token };
};
