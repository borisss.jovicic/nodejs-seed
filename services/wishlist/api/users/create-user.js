const Joi = require('@hapi/joi');
const bcrypt = require('bcrypt');
const db = require('../../repositories');
const userPresenter = require('./user-presenter');
const jwt = require('jsonwebtoken');
const config = require('../../config');

const schema = Joi.object({
  name: Joi.string().required(),
  username: Joi.string().required(),
  password: Joi.string()
    .min(5)
    .max(15)
    .required(),
  gender: Joi.string().required()
});

module.exports = async function createUser(data) {
  const userData = Joi.attempt(data, schema);

  const passwordHashed = await bcrypt.hash(userData.password, 10);

  const id = await db.users.create({
    ...userData,
    password: passwordHashed
  });

  const token = jwt.sign(
    {
      id
    },
    { key: config.jwt.privateKey, passphrase: config.jwt.passPhrase },
    { algorithm: 'RS256' }
  );

  const response = await userPresenter.present([id]);

  return { response, token };
};
