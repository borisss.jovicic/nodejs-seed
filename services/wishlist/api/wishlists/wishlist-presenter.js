const db = require('../../repositories');

async function present(wishlistIds) {
  const wishlist = await db.wishlists.getByIds(wishlistIds);

  return wishlist;
}

module.exports = { present };
