const Joi = require('@hapi/joi');
const db = require('../../repositories');
const wishlistPresenter = require('./wishlist-presenter');

const schema = Joi.object({
  name: Joi.string().required()
}).required();

module.exports = async function createWishlist(data) {
  const wishlistData = Joi.attempt(data, schema);
  const id = await db.wishlists.create(wishlistData);

  const response = await wishlistPresenter.present([id]);

  return response;
};
