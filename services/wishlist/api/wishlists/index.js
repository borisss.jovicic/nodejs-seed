const router = require('express').Router();
const createWishlist = require('./create-wishlist');
const asyncHandler = require('express-async-handler');

router.post(
  '/wishlists',
  asyncHandler(async (req, res) => {
    console.log('req', req.body);
    const response = await createWishlist(req.body);
    console.log('response', response);
    res.send(response);
  })
);

const wishlistRouter = router;

module.exports = wishlistRouter;
