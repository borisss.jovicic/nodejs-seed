const router = require('express').Router();
const createProduct = require('./create-product');
const asyncHandler = require('express-async-handler');
const getById = require('./get-by-id');
const authMiddleware = require('../../middleware/auth-middleware');
const createProductImage = require('./create-product-image');
const asyncBusBoy = require('async-busboy');

router.post(
  '/products',
  authMiddleware,
  asyncHandler(async (req, res) => {
    const response = await createProduct(req.body);
    res.send(response);
  })
);

router.get(
  '/products/:id',
  authMiddleware,
  asyncHandler(async (req, res) => {
    const result = await getById(req.params.id);
    res.send(result);
  })
);

router.post(
  '/products/:id/images',
  asyncHandler(async (req, res) => {
    const { files } = await asyncBusBoy(req);
    const productId = req.params.id;

    const result = await createProductImage(productId, files[0]);

    res.send(200, result);
  })
);

const productRouter = router;

module.exports = productRouter;
