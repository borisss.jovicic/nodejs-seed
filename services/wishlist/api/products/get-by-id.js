const Joi = require('@hapi/joi');
const db = require('../../repositories');
const productPresenter = require('./product-presenter');

const schema = Joi.number().required();

module.exports = async function getById(data) {
  const id = Joi.attempt(data, schema);
  const product = await db.products.getById(id);

  const response = await productPresenter.present([product.id]);

  return response[0];
};
