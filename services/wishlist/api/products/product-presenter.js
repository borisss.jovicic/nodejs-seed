const db = require('../../repositories');

async function present(productIds) {
  const products = await db.products.getByIds(productIds);

  const categoryIds = products.map(product => {
    return product.categoryId;
  });

  const [categories, productImages] = await Promise.all([
    db.productCategories.getByIds(categoryIds),
    db.productImages.getByProductIds(productIds)
  ]);

  const result = products.map(product => {
    const category = categories.find(cat => cat.id === product.categoryId);
    const images = productImages.filter(
      image => image.productId === product.id
    );

    return {
      ...product,
      category,
      images
    };
  });

  return result;
}

module.exports = { present };
