const AWS = require('aws-sdk');
const config = require('../../config');
const uuid = require('uuid/v4');
const path = require('path');
const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');
const db = require('../../repositories');

const s3 = new AWS.S3({
  credentials: {
    accessKeyId: config.aws.accessKeyId,
    secretAccessKey: config.aws.secretAccessKey
  },
  endpoint: config.aws.endpoint,
  s3BucketEndpoint: !!config.aws.endpoint,
  s3ForcePathStyle: true
});

const schema = Joi.object({
  productId: Joi.number().required(),
  url: Joi.string().required()
}).required();

async function createProductImage(productId, file) {
  const objectKey = `${uuid()}${path.extname(file.filename)}`;
  const uploadResult = await s3
    .upload({
      Bucket: config.product.imagesBucket,
      Key: objectKey,
      Body: file
    })
    .promise();

  if (!uploadResult) {
    throw Boom.notFound('Upload failed');
  }

  const data = {
    productId: productId,
    url: uploadResult.Location
  };

  console.log('sta je data', data);

  const productImagesData = Joi.attempt(data, schema);

  const response = await db.productImages.create(productImagesData);

  return response;
}

module.exports = createProductImage;
