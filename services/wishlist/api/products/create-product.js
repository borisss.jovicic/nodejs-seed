const Joi = require('@hapi/joi');
const db = require('../../repositories');
const productPresenter = require('./product-presenter');

const schema = Joi.object({
  name: Joi.string().required(),
  price: Joi.number().required(),
  categoryId: Joi.number().required()
}).required();

module.exports = async function createProduct(data) {
  const productData = Joi.attempt(data, schema);
  const id = await db.products.create(productData);

  const response = await productPresenter.present([id]);

  return response[0];
};
