const config = require('./config');

const logger = require('pino')({
  name: config.app.name
});

module.exports = logger;
