const app = require('express')();
const bodyParser = require('body-parser');
const productRouter = require('./api/products/index');
const wishlistRouter = require('./api/wishlists/index');
const userRouter = require('./api/users/index');
const logger = require('./logger');
const dbErrors = require('db-errors');
const NotFoundError = require('./../core/error-handler');

app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Hello');
});

app.use('/api', productRouter);
app.use('/api', userRouter);
app.use('/api', wishlistRouter);

app.use((err, req, res, next) => {
  if (err.isJoi) {
    console.log(next);
    return res.status(400).send({
      statusCode: 400,
      message: 'Bad Input',
      details: err.message
    });
  }

  if (err instanceof NotFoundError) {
    return res.status(404).send({
      statusCode: 404,
      message: 'Not found',
      details: err.message
    });
  }

  if (err.isBoom) {
    return res.status(err.output.statusCode).send({
      statusCode: err.output.statusCode,
      message: err.output.payload.error,
      details: err.output.payload.message
    });
  }

  if (err.sqlState) {
    const dbErr = dbErrors.wrapError(err);
    return res.status(400).send({
      message: dbErr.name,
      details: dbErr.constraint
    });
  }

  logger.error(err);
  console.log('err', err);

  res.status(500).send({
    message: 'Internal Server Error'
  });
});

module.exports = app;
