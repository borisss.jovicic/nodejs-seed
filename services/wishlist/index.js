const config = require('./config');
const server = require('./server');
const port = config.app.port;
const appName = config.app.name;
const logger = require('./logger');

server.listen(port, () => {
  logger.info(`${appName} listening on port ${port}!`);
});

process.on('uncaughtException', err => {
  logger.error(err);
  process.exit(1);
});

process.on('unhandledRejection', reason => {
  logger.error(reason);
  throw new Error('unhandledRejection');
});
