const jwt = require('jsonwebtoken');
const Boom = require('@hapi/boom');
const config = require('../config');

async function auth(req, res, next) {
  try {
    if (!req.headers.authorization) {
      throw Boom.unauthorized();
    }

    const token = req.headers.authorization.split(' ')[1];

    const payload = await jwt.verify(token, config.jwt.publicKey, {
      algorithms: ['RS256']
    });
    // eslint-disable-next-line require-atomic-updates
    req.userId = payload.id;
    next();
  } catch (err) {
    next(err);
  }
}

module.exports = auth;
