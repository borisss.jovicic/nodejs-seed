module.exports = {
  products: require('./products'),
  productCategories: require('./product-categories'),
  wishlists: require('./wishlists'),
  users: require('./users'),
  productImages: require('./product-images')
};
