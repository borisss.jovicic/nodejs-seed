const knexConfig = require('../../../knexfile');
const knex = require('knex')(knexConfig);

const PRODUCT_IMAGES = 'productImages';

async function create(data) {
  console.log('data', data);
  const result = await knex(PRODUCT_IMAGES).insert(data);

  return result[0];
}

async function getByProductIds(ids = []) {
  const result = await knex(PRODUCT_IMAGES)
    .select('*')
    .whereIn('productId', ids);

  return result;
}

module.exports = { create, getByProductIds };
