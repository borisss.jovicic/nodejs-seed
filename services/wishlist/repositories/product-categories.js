const knexConfig = require('../../../knexfile');
const knex = require('knex')(knexConfig);

const PRODUCTS_CATEGORIES = 'productCategories';

async function create(data) {
  console.log('data', data);
  const result = await knex(PRODUCTS_CATEGORIES).insert(data);

  return result;
}

async function getByIds(ids = []) {
  const result = await knex(PRODUCTS_CATEGORIES)
    .select('*')
    .whereIn('id', ids);

  return result;
}

module.exports = { create, getByIds };
