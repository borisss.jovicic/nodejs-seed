const knexConfig = require('../../../knexfile');
const knex = require('knex')(knexConfig);
// const Boom = require('@hapi/boom');
const NotFoundError = require('../../core/error-handler');

const PRODUCTS = 'products';

async function create(data) {
  console.log('data', data);
  const result = await knex(PRODUCTS).insert(data);

  return result[0];
}

async function getByIds(ids = []) {
  const result = await knex(PRODUCTS)
    .select('*')
    .whereIn('id', ids);

  return result;
}

async function getById(id) {
  const result = await knex(PRODUCTS)
    .first('*')
    .where({ id });

  if (!result) {
    throw new NotFoundError('product not found');
  }

  return result;
}

module.exports = { create, getByIds, getById };
