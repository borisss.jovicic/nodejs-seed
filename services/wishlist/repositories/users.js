const knexConfig = require('../../../knexfile');
const knex = require('knex')(knexConfig);
// const Boom = require('@hapi/boom');

const USERS = 'users';

async function create(data) {
  const result = await knex(USERS).insert(data);

  return result;
}

async function getByIds(ids = []) {
  const result = await knex(USERS)
    .select('*')
    .whereIn('id', ids);

  return result;
}

async function getByUsername(username) {
  const user = await knex(USERS)
    .first('*')
    .where({ username });

  return user;
}

module.exports = { create, getByIds, getByUsername };
