const knexConfig = require('../../../knexfile');
const knex = require('knex')(knexConfig);
// const Boom = require('@hapi/boom');

const WISHLISTS = 'wishlists';

async function create(data) {
  console.log('data', data);
  const result = await knex(WISHLISTS).insert(data);

  return result;
}

async function getByIds(ids = []) {
  const result = await knex(WISHLISTS)
    .select('*')
    .whereIn('id', ids);

  return result;
}

module.exports = { create, getByIds };
