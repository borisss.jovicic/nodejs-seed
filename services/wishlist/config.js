require('dotenv').config();

const { env } = process;

module.exports = {
  app: {
    name: env.APP_NAME,
    port: env.PORT
  },
  db: {
    name: env.DB_NAME,
    user: env.DB_USER,
    host: env.DB_HOST,
    password: env.DB_PASSWORD
  },
  jwt: {
    privateKey: env.JWT_PRIVATE_KEY,
    publicKey: env.JWT_PUBLIC_KEY,
    passPhrase: env.JWT_PASSPHRASE
  },
  aws: {
    accessKeyId: env.AWS_ACCESS_KEY_ID,
    secretAccessKey: env.AWS_SECRET_ACCESS_KEY,
    endpoint: env.AWS_S3_ENDPOINT,
    region: env.AWS_REGION
  },
  product: {
    imagesBucket: env.PRODUCT_IMAGES_BUCKET
  }
};
