const server = require('../server');
const request = require('supertest');
const db = require('../repositories');
const token =
  'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTcyMzY2OTY1fQ.q6tvxJsFKrGFfKKXI5CTlZQDnZCq9NnxKVhZ3aKMOY7Wu7QVlZ9u2qX3gxVJemFWorvgABweujFhIsw01srz1cBgN0tvd5plBaFimikTE5fOWv8A28VzQmtdpD87psfu5drJifux14yQwCpgy-wzFbB2CEQ96D6fTIFTCAaqCimOn6ohz7KnqLCpUQty6bf-p_TRfPYCG3Gj6StyiZn3JjevikZ9Abx58EwV4HTNFYFKlIkZW90zJMy-jNsu0LTQ2UIHrzsy53yFR6Xcitc0WrR1nyP7qfrQL3L1bbFNenbRxnUZ-slMRsBBk89kDaT5V9kWRfyFwrf2I-HX9Wg9xA';

jest.mock('../repositories');

describe('POST /api/products', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('creates products', async () => {
    const product = {
      name: 'Super Product',
      price: 99.99,
      categoryId: 1
    };

    const category = {
      id: 1,
      name: test
    };

    db.products.create.mockResolvedValue(product.id);

    db.products.getByIds.mockResolvedValue([product]);

    db.productCategories.getByIds.mockResolvedValue([category]);

    const response = await request(server)
      .post('/api/products')
      .set('Authorization', token)
      .send({
        name: product.name,
        price: product.price,
        categoryId: product.categoryId
      });

    console.log('response', response);

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      ...product,
      category: {
        id: category.id
      }
    });
    expect(response.body.name).toBe(product.name);
    expect(response.body.price).toBe(product.price);
  });

  it('missing name validation', async () => {
    const product = {
      price: 99.99,
      categoryId: 1
    };
    const response = await request(server)
      .post('/api/products')
      .set('Authorization', token)
      .send(product);

    expect(response.status).toBe(400);
    expect(response.body.message).toMatchInlineSnapshot(`"Bad Input"`);
  });
});
