class NotFoundError extends Error {
  /**
   * Constructs the MyError class
   * @param {String} message an error message
   * @constructor
   */
  constructor(message) {
    super(message);

    console.log(message);

    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
  }
}

module.exports = NotFoundError;
