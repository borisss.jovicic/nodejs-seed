exports.up = function(knex) {
  return knex.schema.alterTable('products', t => {
    t.timestamp('createdAt', { precision: 6 }).defaultTo(knex.fn.now(6));
  });
};

exports.down = function(knex) {
  return knex.schema.alterTable('products', t => {
    t.dropcolumn('createdAt');
  });
};
