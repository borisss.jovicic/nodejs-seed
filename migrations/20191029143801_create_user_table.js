exports.up = function(knex) {
  return knex.schema.createTable('users', t => {
    t.increments('id');
    t.string('name');
    t.string('username');
    t.string('password');
    t.string('gender');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('users');
};
