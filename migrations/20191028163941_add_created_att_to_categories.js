exports.up = function(knex) {
  return knex.schema.alterTable('productCategories', t => {
    t.timestamp('createdAt', { precision: 6 }).defaultTo(knex.fn.now(6));
  });
};

exports.down = function(knex) {
  return knex.schema.alterTable('productCategories', t => {
    t.dropcolumn('createdAt');
  });
};
